//
//  Movie.swift
//  MoviesAPP
//
//  Created by Maritza Rodriguez on 13/12/17.
//  Copyright © 2017 Maritza Rodriguez. All rights reserved.
//

import Foundation
import UIKit

class Movie {
    var movieId:String
    var name:String
    var description:String
    var largeImageURL:String
    var image:UIImage
    
    init(movieId:String, name:String, description:String, largeImageURL: String, image:UIImage) {
        self.movieId = movieId
        self.name = name
        self.description = description
        self.largeImageURL = largeImageURL
        self.image = image
    }
    
    
}
