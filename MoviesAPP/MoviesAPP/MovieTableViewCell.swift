//
//  MovieTableViewCell.swift
//  MoviesAPP
//
//  Created by Maritza Rodriguez on 13/12/17.
//  Copyright © 2017 Maritza Rodriguez. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    var movie:Movie?
    
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var movieName: UILabel!
    
    @IBOutlet weak var movieDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
        
    func fillCellData(){
        
        movieName.text = movie?.name
        movieDescription.text = movie?.description
        
        movieImage.image = movie?.image
    
        // Download only if it hasn't been downloaded yet
            
            backendManager.downloadImage(imageURL: (movie?.largeImageURL)!, completionHanddler: { (image) in
                
                self.movie?.image = image
                
                DispatchQueue.main.async(execute: {
                    
                    self.movieImage.image = self.movie?.image
                })
            })
    }
}
