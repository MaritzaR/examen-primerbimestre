//
//  GlobalVars.swift
//  MoviesAPP
//
//  Created by Maritza Rodriguez on 13/12/17.
//  Copyright © 2017 Maritza Rodriguez. All rights reserved.
//

import Foundation
import UIKit

var movieArray = [Movie]() {
    
    didSet{
        
        NotificationCenter.default.post(name: NSNotification.Name("reloadTVMData"), object: nil, userInfo: nil)
        NotificationCenter.default.post(name: NSNotification.Name("reloadFVMData"), object: nil, userInfo: nil)
        
    }
}

