//
//  BackendManager.swift
//  MoviesAPP
//
//  Created by Maritza Rodriguez on 12/12/17.
//  Copyright © 2017 Maritza Rodriguez. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import CoreData

let backendManager:BackendManager = .shared

class BackendManager{
    
    static let shared = BackendManager()
    static var movies: [NSManagedObject] = []
    var moviesLista = [Movie]()
    
    
    
    // MARK: - Server connections
    
    let movieURL = "https://api.themoviedb.org/3/movie"
    
    func fetchDataFromAPI(url:String , completionHandler: @escaping ([Movie]) -> ()){
    
    let url = movieURL + url
        
    
    Alamofire.request(url).responseJSON { response in
    
    guard let json:[String:Any] = response.result.value as? [String : Any] else {
    return
    }
    
    let items = json["results"] as! NSArray
        
    
    
    var movies = [Movie]()
    
    for item in items {
    
    guard let itemDict = item as? [String:Any] else {
    return
    }
    
    let movieId = String("\(itemDict["id"] ?? "0")")!
    
    
    
    let movie = Movie(movieId: movieId, name: "\(itemDict["original_title"] ?? "n/a")", description: "\(itemDict["overview"] ?? "n/a")", largeImageURL: "\(itemDict["poster_path"] ?? "n/a")", image: #imageLiteral(resourceName: "missingImage.png") )
    
    print("Movie\(movie)")
    
    movies += [movie]
    
    }
    
    completionHandler( movies)
    
    }
    
    }
    
    
    
    func downloadImage(imageURL:String, completionHanddler: @escaping (UIImage) -> ()){
        
        let nuevoURL = "https://image.tmdb.org/t/p/w500\(imageURL)" as String
        
        Alamofire.request(nuevoURL).responseImage { response in
            
            guard let image = response.result.value else {
                return
            }
            
            completionHanddler(image)
            
        }
        
        
    }
    
    
}
