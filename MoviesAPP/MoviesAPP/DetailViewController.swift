//
//  DetailViewController.swift
//  MoviesAPP
//
//   Created by Maritza Rodriguez on 12/12/17.
//  Copyright © 2017 Maritza Rodriguez. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var movie:Movie?
    var movies: [NSManagedObject] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        loadData()
        
        
    }
    
    func loadData(){
        
        titleLabel.text = movie?.name
        descriptionLabel.text = movie?.description
        
   
        
        backendManager.downloadImage(imageURL: (movie?.largeImageURL)!) { (image) in
            
            DispatchQueue.main.async {
                
                self.movieImage.image = image
            }
        }
        
    }
    
    
    
   
    
    
    @IBAction func addToFavourites(_ sender: Any) {
        
       backendManager.saveMovie(newMovie: movie!)
        
    }
    

}
