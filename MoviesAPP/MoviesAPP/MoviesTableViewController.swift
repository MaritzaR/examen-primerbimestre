//
//  MoviesTableViewController.swift
//  MoviesAPP
//
//  Created by Maritza Rodriguez on 13/12/17.
//  Copyright © 2017 Maritza Rodriguez. All rights reserved.
//

import UIKit
import ReachabilitySwift
import CoreData

class MoviesTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{

    var selectedIndex = 0

    

     var movies: [NSManagedObject] = []
    

  
    
  
    @IBOutlet weak var moviesTableView: UITableView!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("reloadTVMData"), object: nil)
        
            
        downloadMovies()
        
    }
    
    
    func downloadMovies(){
        backendManager.fetchDataFromAPI(url: "/popular?api_key=4129571d8d96f11a308560d0c52ece6f&language=es-ES&page=1") { (movies) in
            movieArray = movies
        }
        
    }
    
    
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return movieArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieTVC") as! MovieTableViewCell
       
            
            cell.movie = movieArray[indexPath.row]
        
        cell.fillCellData()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        selectedIndex = indexPath.row
        
        return indexPath
    }
    
 
    
    
    func reloadData(){
        
 
        moviesTableView.reloadData()
        
        
    }
    
 

    
    
   }
